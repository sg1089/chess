package com.example.sgurung4.chess;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static android.media.CamcorderProfile.get;

/**
 * Created by Mynhung on 12/15/2016.
 */

public class PlayBack extends AppCompatActivity{

    private Context context;
    //protected CustomAdapter adapter;

    private ArrayList<String> gameList;
    private ArrayList<Integer> fileIndex = new ArrayList<Integer>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playback_game);
        Intent intent = getIntent();
        /*ListView listView = (ListView) findViewById(R.id.saved_list);
        adapter = new CustomAdapter();
        listView.setAdapter(adapter);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return gameList.size();
        }

        @Override
        public Object getItem(int position) {
            return gameList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;
            View view;
            final String filename = gameList.get(pos);


            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                view = inflater.inflate(R.layout.playback_game, parent, false);
            } else {
                view = convertView;
            }
            /*final TextView saves_filename = (TextView)view.findViewById(R.id.saves_filename);
            saves_filename.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // Rename file
                    return false;
                }
            });
            Button saves_btn_load = (Button)view.findViewById(R.id.saves_btn_load);
            saves_btn_load.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionLoadSave(savefiles.get(pos));
                }
            });
            saves_filename.setText(savefiles.get(pos).getName());

            CheckBox saves_checkBox = (CheckBox)view.findViewById(R.id.saves_checkbox);
            saves_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        selectedFileIndex.add(pos);
                    else
                        selectedFileIndex.remove(pos);
                }
            })

            return view;
        }
    }

    private void optionLoadSave(File currentFile) {
        final String title = currentFile.getName();
        final File file = currentFile;
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(title)
                .setMessage("Do you want to load this game? " + file.getName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), ChessActivity.class);
                        //Bundle bundle = new Bundle();

                        try{//load name of all saved games into gameList
                            File dir = context.getDir("SavedFiles", context.MODE_PRIVATE);
                            String str = "GameName.txt";
                            File saveFile = new File(dir,str);
                            FileInputStream fis = new FileInputStream(saveFile);
                            ObjectInputStream ois = new ObjectInputStream(fis);
                            while(fis.available() > 0){
                                Object ob = new Object();
                                ob = ois.readObject();
                                String s = ob.toString();
                                gameList.add(s);
                            }
                            ois.close();
                            fis.close();
                            Log.i("Load file", "Successful");
                            System.out.println(gameList);
                        }
                        catch(FileNotFoundException e){
                            Log.e("Exception", "File not found: " + e.toString());
                        }catch (IOException e){
                            Log.e("Exception", "Can not read file: " + e.toString());
                        }catch (ClassNotFoundException e){
                            Log.e("Exception", "Class not found: " + e.toString());
                        }

                        //bundle.putInt("numberplayers", 2);
                        //bundle.putBoolean("replay", true);
                        //intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", null)
                .show();*/
    }

}
