package com.example.sgurung4.chess;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

public class HomeScreen extends AppCompatActivity {

    Button startButton;
    Button playBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        Button startButton = (Button) findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeScreen.this, ChessActivity.class);
                startActivity(intent);
            }
        });

        Button playBackButton = (Button) findViewById(R.id.playBackButton);
        playBackButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeScreen.this, PlayBack.class);
                startActivity(intent);
            }

        });
    }

}



























































































































































