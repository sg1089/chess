package com.example.sgurung4.chess;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import chess.chessGame;

public class ChessActivity extends AppCompatActivity {

    private Context context;

    private static chessGame c = null;
    private static View[] location;
    private static String player;
    private static boolean firstPlayerMove;
    private static String piece2;
    private static String piece1;
    public boolean gameOver = false;
    public boolean madeMove = false;
    private static String win;
    private static String eatenPiece;
    private static boolean undo=false, AI=false;
    private static boolean bKingCheckMate = false, wKingCheckMate;
    private static boolean check = false;

    private static ArrayList<String> moves;
    private static ArrayList<String> whitePieces;
    private static ArrayList<String> blackPieces;
    private static ArrayList<Integer>possiblemove1;
    private static ArrayList<Integer>possiblemove2;
    private static ArrayList<String> gameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess);

        Intent intent = getIntent();
    }

    @Override
    protected void onStart() {
        super.onStart();
        context = getApplicationContext();
        c = new chessGame();
        c.startGame(c);
        location = new View[2];
        player = "white";
        firstPlayerMove = true;
        moves = new ArrayList<String>();
        gameList = new ArrayList<String>();
        whitePieces = new ArrayList<String>();
        whitePieces.add(0, "a2"); whitePieces.add(1, "b2"); whitePieces.add(2, "c2"); whitePieces.add(3, "d2");
        whitePieces.add(4, "e2"); whitePieces.add(5, "f2"); whitePieces.add(6, "g2"); whitePieces.add(7, "h2");
        whitePieces.add(8, "a1"); whitePieces.add(9, "b1"); whitePieces.add(10, "c1"); whitePieces.add(11, "d1");
        whitePieces.add(12, "e1"); whitePieces.add(13, "f1"); whitePieces.add(14, "g1"); whitePieces.add(15, "h1");
        blackPieces = new ArrayList<String>();
        blackPieces.add(0, "a8"); blackPieces.add(1, "b8"); blackPieces.add(2, "c8"); blackPieces.add(3, "d8");
        blackPieces.add(4, "e8"); blackPieces.add(5, "f8"); blackPieces.add(6, "g8"); blackPieces.add(7, "h8");
        blackPieces.add(8, "a7"); blackPieces.add(9, "b7"); blackPieces.add(10, "c7"); blackPieces.add(11, "d7");
        blackPieces.add(12, "e7"); blackPieces.add(13, "f7"); blackPieces.add(14, "g7"); blackPieces.add(15, "h7");

        try{//load name of all saved games into gameList
            File dir = context.getDir("SavedFiles", context.MODE_PRIVATE);
            String str = "GameName.txt";
            File saveFile = new File(dir,str);
            FileInputStream fis = new FileInputStream(saveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            while(fis.available() > 0){
                Object ob = new Object();
                ob = ois.readObject();
                String s = ob.toString();
                gameList.add(s);
            }
            ois.close();
            fis.close();
            Log.i("Load file", "Successful");
            System.out.println(gameList);
        }
        catch(FileNotFoundException e){
            Log.e("Exception", "File not found: " + e.toString());
        }catch (IOException e){
            Log.e("Exception", "Can not read file: " + e.toString());
        }catch (ClassNotFoundException e){
            Log.e("Exception", "Class not found: " + e.toString());
        }

        try{//load moves from saved game into moves
            File dir = context.getDir("SavedFiles", context.MODE_PRIVATE);
            String str = "test.txt";
            File saveFile = new File(dir,str);
            FileInputStream fis = new FileInputStream(saveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            while(fis.available() > 0){
                Object ob = new Object();
                ob = ois.readObject();
                String s = ob.toString();
                moves.add(s);
            }
            ois.close();
            fis.close();
            Log.i("Load file", "Successful");
            System.out.println(moves);
        }
        catch(FileNotFoundException e){
            Log.e("Exception", "File not found: " + e.toString());
        }catch (IOException e){
            Log.e("Exception", "Can not read file: " + e.toString());
        }catch (ClassNotFoundException e){
            Log.e("Exception", "Class not found: " + e.toString());
        }

    }

    public void onSelect(View view) {

        if (!gameOver) {
            String position = (String) view.getTag();
            char l1 = position.charAt(0);
            int n1 = Character.getNumericValue(position.charAt(1));

            int col1 = c.convertToArray(l1);
            int row1 = c.convertToArray(n1);
            String currentPiece = c.board[row1][col1];


            if (firstPlayerMove && madeMove == false) {
                if (currentPiece.equals("  ") || currentPiece.equals("##")) {
                    Toast.makeText(ChessActivity.this, "Please select a " + player + " piece", Toast.LENGTH_SHORT).show();
                    return;
                }
                //String currentPiece = c.getPiece(row1, col1);
                if (player.equals("white") && currentPiece.charAt(0) == 'b') {
                    Toast.makeText(ChessActivity.this, "Player 1's turn. Please move a white piece", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(ChessActivity.this, "Please end your turn1", Toast.LENGTH_SHORT).show();
                    return;
                } else if (player.equals("black") && currentPiece.charAt(0) == 'w') {
                    Toast.makeText(ChessActivity.this, "Player 2's turn. Please move a black piece", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(ChessActivity.this, "Please click End Turn2", Toast.LENGTH_SHORT).show();
                    return;
                }
                location[0] = view;
                firstPlayerMove = false;
            } else if (firstPlayerMove == false && madeMove == false) {
                location[1] = view;
                movePiece();
                firstPlayerMove = true;
            } else {
                if (player.equals("white")) {
                    Toast.makeText(ChessActivity.this, "Cannot move twice in a row. Player 2's turn. Please end turn.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChessActivity.this, "Cannot move twice in a row. Player 1's turn. Please end turn.", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Error");
            builder.setMessage("Game Over");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        }
    }

    public void movePiece() {
        piece1 = (String) location[0].getTag();
        Log.d(piece1, "here is piece1: "+piece1);
        System.out.println("piece1 in movePiece(): " + piece1);
        piece2 = (String) location[1].getTag();
        System.out.println("piece2 in movePiece(): " + piece2);
        Log.d(piece2, "here is piece2: "+piece2);

        char l1 = piece1.charAt(0);
        int n1 = Character.getNumericValue(piece1.charAt(1));
        int col1 = c.convertToArray(l1);
        int row1 = c.convertToArray(n1);

        char l2 = piece2.charAt(0);
        int n2 = Character.getNumericValue(piece2.charAt(1));
        int col2 = c.convertToArray(l2);
        int row2 = c.convertToArray(n2);
        String p2=c.getPiece1(row2, col2);
        /*int id1 = location[0].getId();
        int id2 = location[1].getId();*/

        /*if( c.board[row1][col1].charAt(0)!=player.charAt(0) ) {
            return;
        }*/

        // can move piece
        if (c.movePiece(row1, col1, row2, col2) == true) {

            madeMove = true;
            //moves.add(piece1 +" " + piece2);


            int id1 = location[0].getId();
            int id2 = location[1].getId();

            ImageButton original = (ImageButton) findViewById(id1);
            ImageButton moveTo = (ImageButton) findViewById(id2);

            //original.setImageResource(R.drawable.bB);
            //moveTo.setImageResource(R.drawable.black_king);
           /* String moveToTag = (String)moveTo.getTag();
            String moveToPos = moveToTag.substring(0,2);
            String moveToPiece = moveToTag.substring(2,4);
            String originalTag = (String)original.getTag();
            String originalPos = originalTag.substring(0,2);
            String originalPiece = originalTag.substring(2,4);
            eatenPiece = moveToPiece;*/

            String p1 = c.getPiece1(row2, col2);
            int resID1 = getResources().getIdentifier(p1, "drawable", getPackageName());
            int resID2 = getResources().getIdentifier(p1, "drawable", getPackageName());

            eatenPiece = p2;
            //eatenPiece = (String) moveTo.getText();
            if( whitePieces.contains(piece2) ) { whitePieces.remove(piece2); }
            if( blackPieces.contains(piece2) ) { blackPieces.remove(piece2); }
            //moveTo.setText(original.getText());
            moveTo.setImageResource(resID1);
            moveTo.setScaleType(ImageView.ScaleType.FIT_CENTER);
            moveTo.getLayoutParams().width = 90;
            original.setImageDrawable(null);
            original.setScaleType(ImageView.ScaleType.FIT_CENTER);
            original.getLayoutParams().width = 90;
            //original.setText(null);

            madeMove = true;

            int i = 0;
            if (player.equals("white")) {
                i = 1;
            } else {
                i = 2;
            }

            // check if any kings are in checkMate
            if (c.trycheckMate() == true) {

                if (i == 1) {
                    Toast.makeText(ChessActivity.this, "player2(Black King) checkmated! Player 1(White) wins!", Toast.LENGTH_LONG).show();
                    gameOver = true;
                    win = "Player 1(White) won.";
                    save();
                    return;
                } else {
                    Toast.makeText(ChessActivity.this, "player1(White King) checkmated! Player2(Black) wins!", Toast.LENGTH_LONG).show();
                    gameOver = true;
                    win = "Player2 (Black) won.";
                    save();
                    return;
                }
            }

            /*if(player.equals("white")) {
                 player = "black";
            } else { player = "white"; }*/

            if (c.tryCheck() == true) {
                Toast.makeText(ChessActivity.this, "King in Check", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(ChessActivity.this, "Invalid Move 1", Toast.LENGTH_SHORT).show();
        }
        return;

    }

    public void endTurn(View view) {
        if(!gameOver) {
            if (madeMove == true) {
                madeMove = false;
                AI = false;
                moves.add(piece1 + " " + piece2);
                System.out.println("moves: " + moves);
                if (player.equals("white")) {
                    if (whitePieces.contains(piece1)) {
                        int i = whitePieces.indexOf(piece1);
                        whitePieces.set(i, piece2);
                    } else {
                        whitePieces.remove(whitePieces.indexOf(piece1));
                    }
                    System.out.println("whitePieces: " + whitePieces);
                    player = "black";
                } else {
                    if (blackPieces.contains(piece1)) {
                        int i = blackPieces.indexOf(piece1);
                        blackPieces.set(i, piece2);
                    } else {
                        blackPieces.remove(blackPieces.indexOf(piece1));
                    }
                    //blackPieces.add(piece2);
                    System.out.println("blackPieces: " + blackPieces);
                    player = "white";
                }
            } else {
                if (player.equals("white")) {
                    Toast.makeText(ChessActivity.this, "Please move a white piece", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChessActivity.this, "Please move a black piece", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public void undo(View view) {
        if(!gameOver) {
            if (madeMove == true) {
                undo = true;
                int id1, id2;

                if (!AI) {
                    id1 = location[1].getId();
                    id2 = location[0].getId();
                } else {
                    id2 = getResources().getIdentifier(piece1, "id", "com.example.sgurung4.chess");
                    id1 = getResources().getIdentifier(piece2, "id", "com.example.sgurung4.chess");

                }

                ImageButton original = (ImageButton) findViewById(id1);
                ImageButton moveTo = (ImageButton) findViewById(id2);

                char l2 = piece2.charAt(0);
                int n2 = Character.getNumericValue(piece2.charAt(1));
                int col2 = c.convertToArray(l2);
                int row2 = c.convertToArray(n2);

                String play = c.getPiece1(row2, col2);
                int resID2 = getResources().getIdentifier(play, "drawable", getPackageName());

                if (eatenPiece.length() != 0) {
                    int eatenResID=0;
                    if( eatenPiece.equals("##") ) {
                        String change = "button_black";
                        eatenResID = getResources().getIdentifier(change, "drawable", getPackageName());
                    } else {
                        eatenResID = getResources().getIdentifier(eatenPiece, "drawable", getPackageName());
                    }
                    moveTo.setImageResource(resID2);
                    moveTo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    moveTo.getLayoutParams().width = 90;
                    if( eatenPiece.equals("##")) {
                        original.setImageDrawable(null);
                        original.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        original.getLayoutParams().width = 90;
                    } else {
                        original.setImageResource(eatenResID);
                        original.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        original.getLayoutParams().width = 90;
                    }
                } else {
                    moveTo.setImageResource(resID2);
                    moveTo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    moveTo.getLayoutParams().width = 90;
                    original.setImageDrawable(null);
                    original.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    original.getLayoutParams().width = 90;
                }
                //System.out.println("eaten Piece: " + eatenPiece);
                if (eatenPiece.equals("  ") || eatenPiece.equals("##")) {
                    System.out.println("eaten Piece is null");
                }

                if (player.equals("white")) {
                    if (eatenPiece.equals("  ") || eatenPiece.equals("##")) {
                        c.undoPiece(piece2, piece1, "null");
                    } else {
                        if (eatenPiece.charAt(1) == 'p') {
                            c.undoPiece(piece2, piece1, "bp");
                        } else if (eatenPiece.charAt(1) == 'r') {
                            c.undoPiece(piece2, piece1, "br");
                        } else if (eatenPiece.charAt(1) == 'n') {
                            c.undoPiece(piece2, piece1, "bn");
                        } else if (eatenPiece.charAt(1) == 'b') {
                            c.undoPiece(piece2, piece1, "bb");
                        } else if (eatenPiece.charAt(1) == 'q') {
                            c.undoPiece(piece2, piece1, "bq");
                        } else if (eatenPiece.charAt(1) == 'k') {
                            c.undoPiece(piece2, piece1, "bk");
                        }
                    }
                }

                if (player.equals("black")) {
                    if (eatenPiece.equals("  ") || eatenPiece.equals("##")) {
                        c.undoPiece(piece2, piece1, "null");
                    } else {
                        if (eatenPiece.charAt(1) == 'p') {
                            c.undoPiece(piece2, piece1, "wp");
                        } else if (eatenPiece.charAt(1) == 'r') {
                            c.undoPiece(piece2, piece1, "wr");
                        } else if (eatenPiece.charAt(1) == 'n') {
                            c.undoPiece(piece2, piece1, "wn");
                        } else if (eatenPiece.charAt(1) == 'b') {
                            c.undoPiece(piece2, piece1, "wb");
                        } else if (eatenPiece.charAt(1) == 'q') {
                            c.undoPiece(piece2, piece1, "wq");
                        } else if (eatenPiece.charAt(1) == 'k') {
                            c.undoPiece(piece2, piece1, "wk");
                        }
                    }
                }

                undo = false;
                madeMove = false;
                return;

            } else {
                Toast.makeText(ChessActivity.this, "Error: can't undo move", Toast.LENGTH_SHORT).show();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Error");
            builder.setMessage("Game Over");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void save() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChessActivity.this);

        builder.setTitle("Save");
        builder.setMessage(win + " Would you like to save this game?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("Save Game");
                saveGame();
            }
        });

        builder.setNegativeButton("NO", null);

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void saveGame(){

        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);

        saveDialog.setTitle("Save");
        saveDialog.setMessage("Pleae enter a game title");

        final EditText input = new EditText(this);
        saveDialog.setView(input);

        saveDialog.setNegativeButton("Cancel", null);
        saveDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String gameName = input.getText().toString();
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date current = Calendar.getInstance().getTime();
                String date = df.format(current);
                String name_date = gameName + " " + date;
                System.out.println(name_date);
                gameList.add(name_date); //name of game and date it was saved added to gameList
                System.out.println(gameList);

                try{//save moves from certain game
                    File dir = context.getDir("SavedFiles", context.MODE_PRIVATE);
                    File saveFile = new File(dir,gameName+".txt");
                    FileOutputStream fos = new FileOutputStream(saveFile);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    for(int i = 0; i < moves.size(); i++){
                        oos.writeObject(moves.get(i));
                    }
                    oos.close();
                    fos.close();
                }
                catch(IOException e){
                    Log.e("Exception", "File write failed: " + e.toString());
                }

                try{//write name of game to GameName.txt
                    File dir = context.getDir("SavedFiles", context.MODE_PRIVATE);
                    String str = "GameName.txt";
                    File saveFile = new File(dir,str);
                    FileOutputStream fos = new FileOutputStream(saveFile);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    for(int i = 0; i < gameList.size(); i++){
                        oos.writeObject(gameList.get(i));
                    }
                    oos.close();
                    fos.close();
                }catch(IOException e){
                    Log.e("Exception", "File write failed: " + e.toString());
                }
            }
        });

        AlertDialog alert = saveDialog.create();
        alert.show();
    }

    public void draw(View view) {
        if(!gameOver) {
            AlertDialog.Builder drawDialog = new AlertDialog.Builder(ChessActivity.this);

            drawDialog.setTitle("Draw");
            win = "It's a Draw.";

            if (player.equals("white")) {
                drawDialog.setMessage("Player1(white) would like to draw. Do you accept?");
                moves.add("p1 draw");
                System.out.println(moves);
            } else {
                drawDialog.setMessage("Player2(black) would like to draw. Do you accept?");
                moves.add("p2 draw");
                System.out.println(moves);
            }

            drawDialog.setNegativeButton("No", null);
            drawDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    gameOver = true;
                    save();
                }
            });
            AlertDialog alert = drawDialog.create();
            alert.show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Error");
            builder.setMessage("Game Over");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void resign(View view) {
        if(!gameOver) {
            AlertDialog.Builder resignDialog = new AlertDialog.Builder(ChessActivity.this);

            resignDialog.setTitle("Resign");

            if (player.equals("white")) {
                resignDialog.setMessage("Player1(white) resigned. Player2(Black) wins!");
                win = "Player2 won.";
                moves.add("p1 resign");
                System.out.println(moves);
            } else {
                resignDialog.setMessage("Player2(black) resigned. Player1(White) wins!");
                win = "Player1 won.";
                moves.add("p2 resign");
                System.out.println(moves);
            }

            resignDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    gameOver = true;
                    save();
                }
            });
            AlertDialog alert = resignDialog.create();
            alert.show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Error");
            builder.setMessage("Game Over");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void AI(View view) {
        if(!gameOver ) {
            if( !madeMove ) {
                boolean canMove = false;
                AI = true;
                possiblemove1 = new ArrayList<>();
                possiblemove2 = new ArrayList<>();
                //int id = location[0].getId();
                //String pos = (String) location[0].getTag();

                if (player.equals("white")) {
                    for (int k = 0; k < whitePieces.size(); k++) {
                        int counter = 0;
                        //int random_index = new Random().nextInt(whitePieces.size() - 1);
                        int random_index = new Random().nextInt(((whitePieces.size()-1)-0)+1)+0;
                        int id2 = getResources().getIdentifier(whitePieces.get(random_index), "id", "com.example.sgurung4.chess");

                        //Button target = (Button) findViewById(id2);
                        ImageButton target2 = (ImageButton) findViewById(id2);

                        char l2 = whitePieces.get(random_index).charAt(0);
                        int n2 = Character.getNumericValue(whitePieces.get(random_index).charAt(1));
                        int col = c.convertToArray(l2);
                        int row = c.convertToArray(n2);

                        String targetPiece = c.getPiece1(row, col);
                        int targetID = getResources().getIdentifier(targetPiece, "drawable", getPackageName());

                        Log.d(targetPiece, "targetPiece: "+targetPiece);

                        if (targetPiece.charAt(1) == 'p') {
                            randomPawn(row, col);
                        }
                        if (targetPiece.charAt(1) == 'r') {
                            randomRook(row, col);
                        }
                        if (targetPiece.charAt(1) == 'n') {
                            randomKnight(row, col);
                        }
                        if (targetPiece.charAt(1) == 'n') {
                            randomBishop(row, col);
                        }
                        if (targetPiece.charAt(1) == 'q') {
                            randomQueen(row, col);
                        }
                        if (targetPiece.charAt(1) == 'k') {
                            randomKing(row, col);
                        }

                        //System.out.println("randomly chosen white piece: " + target.getText());
                        Log.d("possiblemoves size: ", "possible moves: "+possiblemove1.size());
                        //System.out.println("white possible moves size: " + possiblemove1.size());

                        while (!canMove) {
                            if(possiblemove1.size()==0) { break; }
                            int index = new Random().nextInt(((possiblemove1.size()-1)-0)+1)+0;
                            if(counter==possiblemove1.size() ) { break; }

                            String possibleEatenPiece = c.getPiece1(possiblemove1.get(index), possiblemove2.get(index));
                            String moveToPiece = c.getPiece1(possiblemove1.get(index), possiblemove2.get(index));
                            int moveToID = getResources().getIdentifier(moveToPiece, "drawable", getPackageName());

                            if (c.movePiece(row, col, possiblemove1.get(index), possiblemove2.get(index)) == true) {
                                char movePos1 = c.convertToChar(possiblemove1.get(index), possiblemove2.get(index));
                                int movePos2 = c.convertToInt(possiblemove1.get(index), possiblemove2.get(index));

                                int id = getResources().getIdentifier(movePos1 + Integer.toString(movePos2), "id", "com.example.sgurung4.chess");
                                ImageButton moveTo = (ImageButton) findViewById(id);

                                eatenPiece = possibleEatenPiece;
                                checkMate();
                                if (wKingCheckMate == true) {
                                    if(eatenPiece.equals("##") || eatenPiece.equals("  ")) {
                                        c.undoPiece(whitePieces.get(random_index), movePos1 + Integer.toString(movePos2), "null");
                                    } else {
                                        c.undoPiece(whitePieces.get(random_index), movePos1 + Integer.toString(movePos2), eatenPiece);
                                    }
                                    wKingCheckMate = false;
                                    continue;
                                } else {
                                    madeMove = true;
                                    piece1 = whitePieces.get(random_index);
                                    piece2 = movePos1 + Integer.toString(movePos2);
                                    Log.d("piece1, piece2: ", "piece1: "+piece1+ " piece2: "+piece2);

                                    moveTo.setImageResource(targetID);
                                    moveTo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    moveTo.getLayoutParams().width = 90;
                                    target2.setImageDrawable(null);
                                    target2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    target2.getLayoutParams().width = 90;

                                    return;
                                }
                            } else { counter++;}
                        }
                    }
                } else {
                    for (int k = 0; k < blackPieces.size(); k++) {
                        int counter = 0;
                        //int random_index = new Random().nextInt(blackPieces.size() - 1);
                        int random_index = new Random().nextInt(((blackPieces.size()-1)-0)+1)+0;
                        int id2 = getResources().getIdentifier(blackPieces.get(random_index), "id", "com.example.sgurung4.chess");
                        ImageButton target2 = (ImageButton) findViewById(id2);

                        char l2 = blackPieces.get(random_index).charAt(0);
                        int n2 = Character.getNumericValue(blackPieces.get(random_index).charAt(1));
                        int col = c.convertToArray(l2);
                        int row = c.convertToArray(n2);

                        String targetPiece = c.getPiece1(row, col);
                        int targetID = getResources().getIdentifier(targetPiece, "drawable", getPackageName());

                        if (targetPiece.charAt(1) == 'p') {
                            randomPawn(row, col);
                        }
                        if (targetPiece.charAt(1) == 'r') {
                            randomRook(row, col);
                        }
                        if (targetPiece.charAt(1) == 'n') {
                            randomKnight(row, col);
                        }
                        if (targetPiece.charAt(1) == 'b') {
                            randomBishop(row, col);
                        }
                        if (targetPiece.charAt(1) == 'q') {
                            randomQueen(row, col);
                        }
                        if (targetPiece.charAt(1) == 'k') {
                            randomKing(row, col);
                        }
                        System.out.println("randomly chosen black piece: " + targetPiece);
                        System.out.println("black possible moves size: " + possiblemove1.size());

                        while (!canMove) {
                            if(possiblemove1.size()==0) { break; }
                            //int index = new Random().nextInt(possiblemove1.size() - 1);
                            int index = new Random().nextInt(((possiblemove1.size()-1)-0)+1)+0;
                            if( counter==possiblemove1.size()) {break;}
                            String possibleEatenPiece = c.getPiece1(possiblemove1.get(index), possiblemove2.get(index));

                            String moveToPiece = c.getPiece1(possiblemove1.get(index), possiblemove2.get(index));
                            int moveToID = getResources().getIdentifier(moveToPiece, "drawable", getPackageName());

                            if (c.movePiece(row, col, possiblemove1.get(index), possiblemove2.get(index)) == true) {
                                char movePos1 = c.convertToChar(possiblemove1.get(index), possiblemove2.get(index));
                                int movePos2 = c.convertToInt(possiblemove1.get(index), possiblemove2.get(index));

                                int id = getResources().getIdentifier(movePos1 + Integer.toString(movePos2), "id", "com.example.sgurung4.chess");
                                ImageButton moveTo = (ImageButton) findViewById(id);

                                eatenPiece = possibleEatenPiece;
                                checkMate();
                                if (bKingCheckMate == true) {
                                    if(eatenPiece.equals("##") || eatenPiece.equals("  ")) {
                                        c.undoPiece(whitePieces.get(random_index), movePos1 + Integer.toString(movePos2), "null");
                                    } else {
                                        c.undoPiece(whitePieces.get(random_index), movePos1 + Integer.toString(movePos2), eatenPiece);
                                    }
                                    bKingCheckMate = false;
                                    continue;
                                } else {
                                    madeMove = true;
                                    piece1 = blackPieces.get(random_index);
                                    piece2 = movePos1 + Integer.toString(movePos2);
                                    System.out.println("piece1: "+piece1+ " piece2: "+piece2);

                                    moveTo.setImageResource(targetID);
                                    moveTo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    moveTo.getLayoutParams().width = 90;

                                    target2.setImageDrawable(null);
                                    target2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    target2.getLayoutParams().width = 90;

                                    canMove = true;
                                    return;
                                }
                            } else {
                                counter++;
                            }
                        }
                    }
                }
            } if (player.equals("white")) {
                Toast.makeText(ChessActivity.this, "Player 2's turn. Please end turn.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ChessActivity.this, "Player 1's turn. Please end turn.", Toast.LENGTH_SHORT).show();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Error");
            builder.setMessage("Game Over");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void randomPawn(int row, int col) {

        if(player.equals("white")) {
                if( row==6 ) {  possiblemove1.add(row-2); possiblemove2.add(col); }
                if( row!=0 ) { possiblemove1.add(row - 1); possiblemove2.add(col); }
                if( col!=7 && row!=0) { possiblemove1.add(row-1); possiblemove2.add(col+1);  }
                if( col!=0 && row!=0 ) { possiblemove1.add(row - 1); possiblemove2.add(col - 1);  }
        }

        if(player.equals("black")) {
                if( row==1 ) { possiblemove1.add(row+2); possiblemove2.add(col);  }
                if( row!=7) { possiblemove1.add(row+1); possiblemove2.add(col);  }
                if( row!=7 && col!=7) { possiblemove1.add(row+1); possiblemove2.add(col+1);  }
                if( row!=7 && col!=0) { possiblemove1.add(row+1); possiblemove2.add(col-1);  }
        }

    }

    public void randomRook(int row, int col) {

        if( row!=7) {
            for (int i = row + 1; i < 8; i++) {
                possiblemove1.add(i);
                possiblemove2.add(col);
            }
        }
        if( row!=0 ) {
            for (int i = row - 1; i >= 0; i--) {
                possiblemove1.add(i);
                possiblemove2.add(col);
            }
        }
        if( col!=7 ) {
            for (int i = col + 1; i < 8; i++) {
                possiblemove1.add(row);
                possiblemove2.add(i);
            }
        }
        if( col!=0 ) {
            for (int i = col - 1; i >= 0; i--) {
                possiblemove1.add(row);
                possiblemove2.add(i);
            }
        }
    }

    public void randomBishop(int row, int col) {

        for( int j=col+1, i=row+1; j<8 && i<8; j++, i++) {
            possiblemove1.add(i); possiblemove2.add(j);
        }
        for( int j=col-1, i=row+1; j>-1 && i<8; j--, i++ ) {
            possiblemove1.add(i); possiblemove2.add(j);
        }
        for( int j=col-1, i=row-1; j>-1 && i>-1; j--, i--) {
            possiblemove1.add(i); possiblemove2.add(j);
        }
        for( int j=col+1, i=row-1; j<8 && i>-1; j++, i--) {
            possiblemove1.add(i); possiblemove2.add(j);
        }

    }

    public void randomQueen(int row, int col) {
        randomBishop(row, col); randomRook(row, col);
    }

    public void randomKing(int row, int col) {
        if( row!=7 ) {
            possiblemove1.add(row + 1); possiblemove2.add(col);
        }
        if( col!=7 && row!=7) {
            possiblemove1.add(row + 1); possiblemove2.add(col + 1);
        }
        if( col!=0 && row!=7 ) {
            possiblemove1.add(row + 1); possiblemove2.add(col - 1);
        }
        if( row!=0 ) {
            possiblemove1.add(row - 1); possiblemove2.add(col);
        }
        if(row!=0 && col!=7 ) {
            possiblemove1.add(row - 1); possiblemove2.add(col + 1);
        }
        if( row!=0 && col!=0 ) {
            possiblemove1.add(row - 1); possiblemove2.add(col - 1);
        }
        if( col!= 0 ) {
            possiblemove1.add(row); possiblemove2.add(col - 1);
        }
        if( col!= 7 ) {
            possiblemove1.add(row); possiblemove2.add(col + 1);
        }
    }

    public void randomKnight(int row1, int col1) {
        boolean canMove = false;
        //if( row1>=2 && col1<=6 && row1<=5) {
        if( row1>=2 && row1<=5 ) {
            if (col1 != 7) {
                possiblemove1.add(row1 - 2); possiblemove2.add(col1 + 1);
                possiblemove1.add(row1 + 2); possiblemove2.add(col1 + 1);
            } if (col1 != 0) {
                possiblemove1.add(row1 - 2); possiblemove2.add(col1 - 1);
                possiblemove1.add(row1 + 2); possiblemove2.add(col1 - 1);
            } if (col1 < 6) {
                possiblemove1.add(row1 - 1); possiblemove2.add(col1 + 2);
                possiblemove1.add(row1 + 1); possiblemove2.add(col1 + 2);
            } if (col1 > 1) {
                possiblemove1.add(row1 - 1); possiblemove2.add(col1 - 2);
                possiblemove1.add(row1 + 1); possiblemove2.add(col1 - 2);
            }
        }
        if( row1==0 || row1==1) {
            if( col1<6 ) { possiblemove1.add(row1+1); possiblemove2.add(col1+2); }
            if( col1>1 ) { possiblemove1.add(row1+1); possiblemove2.add(col1-2); }
            if( col1!=0) { possiblemove1.add(row1+2); possiblemove2.add(col1-1); }
            if( col1!=7) { possiblemove1.add(row1+2); possiblemove2.add(col1+1); }

            if( row1==1 ) {
                if( col1<6 ) { possiblemove1.add(row1-1); possiblemove2.add(col1+2); }
                if( col1>1 ) { possiblemove1.add(row1-1); possiblemove2.add(col1-2); }

            }
        }
        if( row1==6 || row1==7) {
            if( col1!=0 ) { possiblemove1.add(row1-2); possiblemove2.add(col1-1); }
            if( col1!=7 ) { possiblemove1.add(row1-2); possiblemove2.add(col1+1); }
            if( col1<6 ) { possiblemove1.add(row1-1); possiblemove2.add(col1+2); }
            if( col1>1 ) { possiblemove1.add(row1-1); possiblemove2.add(col1-2); }

            if( row1==6 ) {
                if( col1<6 ) { possiblemove1.add(row1+1); possiblemove2.add(col1+2); }
                if( col1>1 ) { possiblemove1.add(row1+1); possiblemove2.add(col1-2); }
            }
        }

        //System.out.println("possible moves1: " + possiblemove1);
        //System.out.println("possible moves2: " + possiblemove2);

        /*while( !canMove) {
            int index = new Random().nextInt(possiblemove1.size());
            if( c.movePiece(row1, col1, possiblemove1.get(index), possiblemove2.get(index)) == true) {
                madeMove = true;

                char movePos1 = c.convertToChar(possiblemove1.get(index), possiblemove2.get(index));
                int movePos2 = c.convertToInt(possiblemove1.get(index), possiblemove2.get(index));

                //System.out.println("move pos: " + movePos1+movePos2+"");
                //ViewGroup v = (ViewGroup) findViewById(R.id.activity_chess);
                int id1 = location[0].getId();
                //System.out.println("id1: " + id1);
                int id2 = getResources().getIdentifier(movePos1+Integer.toString(movePos2), "id", "com.example.sgurung4.chess");
                //System.out.println("id2: " + id2);

                Button original = (Button) findViewById(id1);
                Button moveTo = (Button) findViewById(id2);

                //System.out.println("original button text: " + original.getText());
                //System.out.println("moveTo button text: " + moveTo.getText());


                moveTo.setText(original.getText());
                original.setText(null);
                /*eatenPiece = (String) moveTo.getText();
                moveTo.setText(original.getText());
                original.setText(null);*/

                //checkMate();
                canMove = true;
            /*}
        }*/
        return;
    }

    public void checkMate() {
        int i = 0;
        if (player.equals("white")) {
            i = 1;
        } else {
            i = 2;
        }
        // check if any kings are in checkMate
        if (c.trycheckMate() == true) {
            if (i == 1) {
                if( AI ) { bKingCheckMate = true; return; }
                Toast.makeText(ChessActivity.this, "player2(Black King) checkmated! Player 1(White) wins!", Toast.LENGTH_LONG).show();
                gameOver = true;
                win = "Player 1(White) won.";
                save();

            } else {
                if( AI) { wKingCheckMate = true; return; }
                Toast.makeText(ChessActivity.this, "player1(White King) checkmated! Player2(Black) wins!", Toast.LENGTH_LONG).show();
                gameOver = true;
                win = "Player2 (Black) won.";
                save();
            }
        }

        if (c.tryCheck() == true) {
            Toast.makeText(ChessActivity.this, "King in Check", Toast.LENGTH_SHORT).show();
        }
    }

}































































































































































































































