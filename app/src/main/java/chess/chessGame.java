package chess;

 import android.view.View;

 import java.util.Arrays;
 import java.util.LinkedList;
 import java.util.List;
 import java.util.Scanner;

public class chessGame{

    public static String board[][]; //initializes the game board
    public static boolean end = false; //tells us if game has ended or not
    private static int counter = 0; //determines turn

    /*
     * These variables are used for castling.
     */
    private static boolean hasWKingMoved = false, hasBKingMoved = false;
    private static boolean right_WRookMoved = false, left_WRookMoved = false;
    private static boolean right_BRookMoved = false, left_BRookMoved = false;

    /*
     * These variables are used for
     * check purposes.
     */
    private static int tracker = 0;
    private static boolean isKingSafe = false, possibleCheckMate = false;
    public static boolean bKingCheck1 = false, wKingCheck1 = false, bKingCheck2=false, wKingCheck2=false;
    private static String threatPiece;
    private static int threatPiecerow = 0, threatPiececol=0;

    /**
     * These global variables are used in
     * the method checkMate() to see if
     * the game is in checkmate
     */
    public static int wKingrow = 7;
    public static int wKingcol = 4;
    public static int bKingrow = 0;
    public static int bKingcol = 4;

    public chessGame() {
        board = new String[8][8];
    }

    public void startGame(chessGame c ) {
        fillBoard();
        drawBoard();
    }
    /**
     * Fill the game board with the initial pieces and spaces
     */

    public static void fillBoard()
    {
        board[0][0] = "br";
        board[0][1] = "bn";
        board[0][2] = "bb";
        board[0][3] = "bq";
        board[0][4] = "bk";
        board[0][5] = "bb";
        board[0][6] = "bn";
        board[0][7] = "br";

        for(int i = 0; i < 8; i++)
        {
            board[1][i] = "bp";
        }

        for(int i=1; i<8; i+=2)
        {
            board[2][i] = "##";
            board[3][i] = "  ";
            board[4][i] = "##";
            board[5][i] = "  ";
        }
        for(int i=0; i<8; i+=2)
        {
            board[2][i] = "  ";
            board[3][i] = "##";
            board[4][i] = "  ";
            board[5][i] = "##";
        }
        for(int i = 0; i < 8; i++)
        {
            board[6][i] = "wp";
        }

        board[7][0] = "wr";
        board[7][1] = "wn";
        board[7][2] = "wb";
        board[7][3] = "wq";
        board[7][4] = "wk";
        board[7][5] = "wb";
        board[7][6] = "wn";
        board[7][7] = "wr";


    }

    /**
     * Prints out the game board
     */
    public static void drawBoard()
    {
        int row = 8;
        for(int i=0; i<8; i++)
        {
            for(int j=0; j<8; j++)
                System.out.print(board[i][j] + " ");

            System.out.print(row);
            System.out.println("");
            row--;
        }
        char[] letters = {'a','b','c','d','e','f','g','h'};
        System.out.print(" ");
        for(int i=0; i<8; i++)
            System.out.print(letters[i] + "  ");
        System.out.println("");
    }

    public static String getPiece1(int row, int col) {
        String piece1 = board[row][col];
        System.out.println("piece1: "+piece1);
        return piece1;
    }

    public static char convertToChar(int row, int col) {
        char pos='a';

        if(col==0) { pos='a'; }
        if(col==1) { pos='b'; }
        if(col==2) { pos='c'; }
        if(col==3) { pos='d'; }
        if(col==4) { pos='e'; }
        if(col==5) { pos='f'; }
        if(col==6) { pos='g'; }
        if(col==7) { pos='h'; }

        char position = pos;
        //System.out.println("pos: " + pos);
        return position;
    }

    public static int convertToInt(int row, int col) {
        int loc=0;

        if( row==0 ) { loc=8; }
        if( row==1 ) { loc=7; }
        if( row==2 ) { loc=6; }
        if( row==3 ) { loc=5; }
        if( row==4 ) { loc=4; }
        if( row==5 ) { loc=3; }
        if( row==6 ) { loc=2; }
        if( row==7 ) { loc=1; }

        //System.out.println("row: " + loc);
        return loc;
    }

    /**
     * Converts lettered column into corresponding
     * index for the game board array
     * @param c2 the lettered column for the position of a chess piece
     * @return corresponding index in the game board array
     */
    public static int convertToArray(char c2)
    {
        char a = 'a';
        char b = 'b';
        char c = 'c';
        char d = 'd';
        char e = 'e';
        char f = 'f';
        char g = 'g';
        if(c2 == a)
            return 0;
        else if(c2 == b)
            return 1;
        else if(c2 == c)
            return 2;
        else if(c2 == d)
            return 3;
        else if(c2 == e)
            return 4;
        else if(c2 == f)
            return 5;
        else if(c2 == g)
            return 6;
        else
            return 7;
    }

    /**
     * Converts numbered row into corresponding
     * index for the game board array
     * @param c numbered row for the position of a chess piece
     * @return corresponding index in the game board array
     */
    public static int convertToArray(int c)
    {
        if(c == 8)
            return 0;
        else if(c == 7)
            return 1;
        else if(c == 6)
            return 2;
        else if(c == 5)
            return 3;
        else if(c == 4)
            return 4;
        else if(c == 3)
            return 5;
        else if(c == 2)
            return 6;
        else
            return 7;
    }

    /**
     * Fills the space with either "  " or ##
     * after a piece has been moved
     * @param row1 row where the piece moved from
     * @param col1 column where the piece moved from
     */
    public static void fillSpace(int row1, int col1)
    {
        if(row1%2 == 0)
        {
            if(col1%2 == 0)
                board[row1][col1] = "  ";
            else
                board[row1][col1] = "##";
        }
        else
        {
            if(col1%2 == 0)
                board[row1][col1] = "##";
            else
                board[row1][col1] = "  ";
        }
    }

    public static void undoPiece(String pos1, String pos2, String eatenPiece) {
        char l1 = pos1.charAt(0);
        int n1 = Character.getNumericValue(pos1.charAt(1));
        int col1 = convertToArray(l1);
        int row1 = convertToArray(n1);

        char l2 = pos2.charAt(0);
        int n2 = Character.getNumericValue(pos2.charAt(1));
        int col2 = convertToArray(l2);
        int row2 = convertToArray(n2);

        if(!eatenPiece.equals("null")) {
            String temp = board[row1][col1];
            board[row1][col1] = eatenPiece;
            board[row2][col2] = temp;
        } else {
            String temp = board[row1][col1];
            board[row2][col2] = temp;
            fillSpace(row1, col1);
        }

        drawBoard();
        counter--;

        return;
    }

    /**
     * Move a chess piece from its original position
     * to the designated position
     * @param row1 row of original position
     * @param col1 column of original position
     * @param row2 row of designated position
     * @param col2 column of designated position
     */
    public static boolean movePiece(int row1, int col1, int row2, int col2)
    {
        String temp = board[row1][col1];
        if(temp.charAt(1) == 'p')
            if(movePawn(row1, col1, row2, col2) == true) { return true; }
            else { return false; }
        else if(temp.charAt(1) == 'r')
            if( moveRook(row1, col1, row2, col2) == true ) { return true; }
            else { return false; }
        else if (temp.charAt(1) == 'b')
            if( moveBishop(row1, col1, row2, col2, temp) == true ) { return true; }
            else { return false;}
        else if(temp.charAt(1) == 'q' )
        {
            if( Math.abs(row2 - row1) == Math.abs(col2 - col1) )  {   if(moveBishop(row1, col1, row2, col2, temp)==true){return true;} else {return false; } }
            else if( row1==row2 || col1==col2 )   {  if( moveRook(row1, col1, row2, col2) == true) { return true; }  else { return false; }  }
            else { System.out.println("error"); return false;  }
        }
        else if(temp.charAt(1) == 'k' )
        {
            if( moveKing(row1, col1, row2, col2, temp) == true ) { return true; } else { return false; }
        }
        else if(temp.charAt(1) == 'r' )
        {
            if( moveRook(row1, col1, row2, col2) == true ) { return true; } else { return false; }
        }
        else if(temp.charAt(1) == 'n' )
        {
            if( moveKnight(row1, col1, row2, col2) == true ) { return true; } else { return false; }
        }
        else
        {
            System.out.println("Illegal move, try again");
            return false;
        }
    }

    /**
     * Move rook to designated position
     *
     * @param row1 row of rook position
     * @param col1 column of rook position
     * @param row2 row of designated position
     * @param col2 column of designated position
     */
    private static boolean moveRook(int row1, int col1, int row2, int col2)
    {
        String piece = board[row1][col1];
        String moveTo = board[row2][col2];
        String temp;
        int rdiff = row2-row1;
        int cdiff = col2-col1;

        //starting spot and ending spot is the same
        if(row1 == row2 && col1 == col2)
        {
            System.out.println("Illegal move, try again");
            return false;
        }
        //can only move along the same row or col
        else if(row1 != row2 && col1 != col2)
        {
            System.out.println("Illegal move, try again");
            return false;
        }
        //moving along the row
        else if(row1 == row2 || col1 == col2)
        {
            int c;
            if(cdiff > 0)
            {
                for(c = col1+1; c < col2; c++)
                {
                    temp = board[row1][c];
                    if(temp.charAt(0) == 'b' || temp.charAt(0) == 'w')
                    {
                        System.out.println("Illegal move, try again");
                        return false;
                    }
                }
            }
            if(cdiff < 0)
            {
                for(c = col1-1; c > col2; c--)
                {
                    temp = board[row1][c];
                    if(temp.charAt(0) == 'b' || temp.charAt(0) == 'w')
                    {
                        System.out.println("Illegal move, try again");
                        return false;
                    }
                }
            }
            if(rdiff < 0)
            {
                for(c = row1-1; c > row2; c--)
                {
                    temp = board[c][col1];
                    if(temp.charAt(0) == 'b' || temp.charAt(0) == 'w')
                    {
                        System.out.println("Illegal move, try again");
                        return false;
                    }
                }
            }
            if(rdiff > 0)
            {
                for(c = row1+1; c < row2; c++)
                {
                    temp = board[c][col1];
                    if(temp.charAt(0) == 'b' || temp.charAt(0) == 'w')
                    {
                        System.out.println("Illegal move, try again");
                        return false;
                    }
                }
            }
            temp = board[row2][col2];

            // Rook is white
            if(piece.charAt(0) == 'w')
            {
                if(moveTo.charAt(0) == 'w')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
                else
                {
                    if( row1==7 && col1==7 )  {  right_WRookMoved = true;  }
                    if( row1==7 && col1==0 )  {  left_WRookMoved = true;   }
                    board[row2][col2] = piece;
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;

                }
            }

            // Rook is black
            else if(piece.charAt(0) == 'b')
            {
                if(moveTo.charAt(0) == 'b')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
                else
                {
                    if( row1==0 && col1==0 )  {  left_BRookMoved = true;   }
                    if( row1==0 && col1==7 )  {  right_BRookMoved = true;  }
                    board[row2][col2] = piece;
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
            }
            else
            {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
        else
        {
            System.out.println("Illegal move, try again");
            return false;
        }

    }

    /**
     * Move king to designated position
     *
     * @param row1 row of king
     * @param col1 column of king
     * @param row2 row of designated position
     * @param col2 column of designated position
     * @param piece determines if piece is black or white
     */
    private static boolean moveKing(int row1, int col1, int row2, int col2, String piece)
    {

        if( row1==row2 && col1==col2 )  {   System.out.println( "Illegal move, not allowed");  return false;   }

        String piece2 = board[row2][col2];
        int row_diff = row2 - row1;
        int col_diff = col2 - col1;

        if( Math.abs(row_diff)>1 || Math.abs(col_diff)>2 )  { System.out.println( "Illegal move, try again here");  return false;  }

        // castling
        if( row_diff==0 && Math.abs(col_diff)==2 )
        {
            // white King
            if( piece.charAt(0)=='w' )
            {
                if( !hasWKingMoved ) {
                    if( col_diff>0 && row_diff==0 ) {
                        if( !right_WRookMoved ) {
                            // can castle white king's right side
                            if( board[row2][col1+1]=="  " && board[row2][col1+2]=="##" && board[row2][col1+3]=="wr") {
                                board[row2][col2] = piece;
                                fillSpace(row1, col1);
                                board[row2][col1+1] = "wr";
                                fillSpace(row2, col1+3);
                                hasWKingMoved = true;
                                right_WRookMoved = true;
                                counter++;
                                System.out.println( "" );
                                drawBoard();
                                wKingrow = row2;
                                wKingcol = col2;
                                return true;
                            } else {   System.out.println( "Illegal move, try again");  return false;   }
                            // right white rook already moved, so castling not possible
                        } else {  System.out.println( "Illegal move, try again"); return false;  }

                    } else if( col_diff<0 && row_diff==0 ) {
                        if( !left_WRookMoved ) {
                            // can castle white king's left side
                            if( board[row2][col1-1]=="  " && board[row2][col1-2]=="##" && board[row2][col1-3]=="  " && board[row2][col1-4]=="wr") {
                                board[row2][col2] = piece;
                                fillSpace(row1, col1);
                                board[row2][col1-1] = "wr";
                                fillSpace(row2, col1-4);
                                hasWKingMoved = true;
                                left_WRookMoved = true;
                                counter++;
                                System.out.println( "" );
                                drawBoard();
                                wKingrow = row2;
                                wKingcol = col2;
                                return true;
                            } else {  System.out.println( "Illegal move, try again");  return false;  }
                            // left white rook already moved, so castling not possible
                        } else { System.out.println( "Illegal move, try again");   return false;   }
                    }
                }
                //  white king already moved, so castling not possible
                else {   System.out.println( "Illegal move, try again");  return false;   }

            }

            // black King
            else if( piece.charAt(0)=='b' )
            {
                if( !hasBKingMoved ) {
                    if( col_diff>0 && row_diff==0 ) {
                        if( !right_BRookMoved ) {
                            // can castle black king's right side
                            if( board[row2][col1+1]=="##" && board[row2][col1+2]=="  " && board[row2][col1+3]=="br") {
                                board[row2][col2] = piece;
                                fillSpace(row1, col1);
                                board[row2][col1+1] = "br";
                                fillSpace(row2, col1+3);
                                hasBKingMoved = true;
                                right_BRookMoved = true;
                                counter++;
                                System.out.println("  ");
                                drawBoard();
                                bKingrow = row2;
                                bKingcol = col2;
                                return true;
                            } else {   System.out.println( "Illegal move, try again");  return false;   }
                            // black right rook already moved, so castling not possible
                        } else {  System.out.println( "Illegal move, try again"); return false;  }

                    } else if( col_diff<0 && row_diff==0 ) {
                        if( !left_BRookMoved ) {
                            // can castle black king's left side
                            if( board[row2][col1-1]=="##" && board[row2][col1-2]=="  " && board[row2][col1-3]=="##" && board[row2][col1-4]=="br") {
                                board[row2][col2] = piece;
                                fillSpace(row1, col1);
                                board[row2][col1-1] = "br";
                                fillSpace(row2, col1-4);
                                hasBKingMoved = true;
                                left_BRookMoved = true;
                                counter++;
                                System.out.println("  ");
                                drawBoard();
                                bKingrow = row2;
                                bKingcol = col2;
                                return true;
                            } else {  System.out.println( "Illegal move, try again");  return false;  }
                            // black left rook already moved, so castling not possible
                        } else { System.out.println( "Illegal move, try again");   return false;   }
                    }
                }
                //  black king already moved, so castling not possible
                else {   System.out.println( "Illegal move, try again");  return false;   }

            } else {  System.out.println( "Illegal move, try again");  return false;   }
        }

        else {
            // move white King
            if( piece.charAt(0)=='w') {
                if( piece2.charAt(0) == 'b' || piece2=="  " || piece2=="##" ) {
                    board[row2][col2] = piece;
                    fillSpace(row1, col1);
                    hasWKingMoved = true;
                    counter++;
                    System.out.println("  ");
                    drawBoard();
                    wKingrow = row2;
                    wKingcol = col2;
                    return true;
                }  else  {   System.out.println( "Illegal move, try again");  return false; }
            }
            // move black King
            else if( piece.charAt(0)=='b') {
                if( piece2.charAt(0) == 'w' || piece2=="  " || piece2=="##" ) {
                    board[row2][col2] = piece;
                    fillSpace(row1, col1);
                    hasBKingMoved = true;
                    counter++;
                    System.out.println("  ");
                    drawBoard();
                    bKingrow = row2;
                    bKingcol = col2;
                    return true;
                }  else  {   System.out.println( "Illegal move, try again"); return false; }
            }
        } return false;

    }

    /**
     * Move knight to designated position
     * @param row1 row of knight
     * @param col1 column of knight
     * @param row2 row of designated position
     * @param col2 column of designated position
     */
    private static boolean moveKnight(int row1, int col1, int row2, int col2) {
        String piece = board[row1][col1];
        String piece2 = board[row2][col2];
        int row_diff = Math.abs(row2 - row1);
        int col_diff = Math.abs(col2 - col1);

        if( (row_diff==2 && col_diff==1) || (row_diff==1 && col_diff==2) )
        {

            if( piece2.charAt(0)==piece.charAt(0) ) {
                System.out.println( "Illegal move, please try again." );
                return false;
            } else {
                board[row2][col2] = piece;
                fillSpace(row1, col1);
                counter++;
                System.out.println("  ");
                drawBoard();
                return true;
            }

        } else {
            System.out.println( "Illegal move, please try again." );
            return false;
        }
    }

    /**
     * Move pawn to designated position
     *
     * @param row1 row of pawn
     * @param col1 column of pawn
     * @param row2 row of designated position
     * @param col2 column of designated position
     */
    private static boolean movePawn(int row1, int col1, int row2, int col2)
    {
        char p = 'z';
        String piece = board[row1][col1];
        int diff = row2 - row1;
        int cdiff = Math.abs(col2 -col1);
        if(piece.equals("wp"))
        {	//pawn can move 2 spaces on first move
            if(row1 == 6 && row2 == 4 && col1 == col2 && (board[5][col1] == "  " || board[5][col1] == "##"))
            {
                if(board[row2][col2].charAt(0) == 'w')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
                else
                {
                    board[row2][col2] = "wp";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
            }
            //moves forward one place
            else if(diff == -1 && col1 == col2 && row2 != 7 && row2 != 0)
            {
                if(board[row2][col2].charAt(0) == 'w' || board[row2][col2].charAt(0) == 'b')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                } else {

                    board[row2][col2] = "wp";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
            }
            //promoting the pawn
            else if(row2 == 0 && diff == -1 && cdiff == 1 && board[row2][col2].charAt(0) != 'w')
            {
                if(p == 'R')
                {
                    board[row2][col2] = "wr";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'N')
                {
                    board[row2][col2] = "wn";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'B')
                {
                    board[row2][col2] = "wb";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'Q' || p == ' ')
                {
                    board[row2][col2] = "wq";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
            }//capturing opponent's piece
            else if(diff == -1 && cdiff == 1 && board[row2][col2].charAt(0) == 'b')
            {
                board[row2][col2] = "wp";
                fillSpace(row1, col1);
                counter++;
                System.out.println("");
                drawBoard();
                return true;
            }
            else
            {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
        else
        {//moving two places on first turn
            if(row1 == 1 && row2 == 3 && col1 == col2 && (board[2][col1] == "  " || board[2][col1] == "##"))
            {
                if(board[row2][col2].charAt(0) == 'b')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
                else

                {
                    board[row2][col2] = "bp";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
            }//moving forward one place
            else if(diff == 1 && col1 == col2 && row2 != 0 && row2 != 7)
            {
                if(board[row2][col2].charAt(0) == 'b' || board[row2][col2].charAt(0) == 'w')
                {
                    System.out.println("Illegal move, try again");
                    return false;
                } else {

                    board[row2][col2] = "bp";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
            }//promoting the pawn
            else if(row2 == 7 && diff == 1 && cdiff == 1 && board[row2][col2].charAt(0) != 'b')
            {
                if(p == 'R')
                {
                    board[row2][col2] = "br";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'N')
                {
                    board[row2][col2] = "bn";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'B')
                {
                    board[row2][col2] = "bb";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else if(p == 'Q' || p == ' ')
                {
                    board[row2][col2] = "bq";
                    fillSpace(row1, col1);
                    counter++;
                    System.out.println("");
                    drawBoard();
                    return true;
                }
                else
                {
                    System.out.println("Illegal move, try again");
                    return false;
                }
            }//capturing opponent's piece
            else if(diff == 1 && cdiff == 1 && board[row2][col2].charAt(0) == 'w')
            {
                board[row2][col2] = "bp";
                fillSpace(row1, col1);
                counter++;
                System.out.println("");
                drawBoard();
                return true;
            }
            else
            {
                System.out.println("Illegal move, try again");
                return false;
            }
        }
    }

    /**
     * Move bishop to designated position
     *
     * @param row1 row of bishop
     * @param col1 column of bishop
     * @param row2 row of designated position
     * @param col2 column of designated position
     * @param piece determines if piece is black or white
     */
    private static boolean moveBishop(int row1, int col1, int row2, int col2, String piece)
    {
        String piece2 = board[row2][col2];
        int row_diff = row2 - row1;
        int col_diff = col2 - col1;
        int diff1, diff2;

        // user doesn't move bishop
        if( row1==row2 || col1==col2 )  {  System.out.println( "Illegal move, try again" );  return false;   }

        // invalid bishop move
        if( Math.abs(row_diff) != Math.abs(col_diff) )  {  System.out.println( "Illegal move, try again" );  return false;   }

        if( row_diff > 0 ) {   diff1 = 1;  }  else  {  diff1 = -1;  }
        if( col_diff > 0 ) {   diff2 = 1;  }  else  {  diff2 = -1;  }

        int y = col1 + diff2;

        for(int x=row1+diff1; x!=row2; x+=diff1 )
        {
            // bishop can't leap over other pieces
            if( board[x][y] != "  " && board[x][y] != "##")
            {
                System.out.println( "Illegal move, try again" );
                return false;
            }

            y += diff2;
        }
        //white bishop
        if( piece.charAt(0) == 'w' )
        {
            if( piece2.charAt(0) != 'w' )
            {
                board[row2][col2] = piece;
                fillSpace(row1, col1);
                counter++;
                System.out.println("  ");
                drawBoard();
                return true;
            }
        }
        else
        {    // black bishop
            if( piece2.charAt(0) != 'b' )
            {
                board[row2][col2] = piece;
                fillSpace(row1, col1);
                counter++;
                System.out.println("  ");
                drawBoard();
                return true;
            }
        }
        return false;
    }

    /**
     * Checks to see if opponent's king is in a state of checkmate
     * by checking to see if the king has any possible moves
     * to get out of check
     *
     * @param color determines the color of the piece
     * @param row1 row of the opponent's king
     * @param col1 column of the opponent's king
     * @return true if opponent is checkmated
     */
    public static int checkMate(char color, int row1, int col1) {

        int p = 0;
        int king = 0;
        int filled = 0;
        try
        {
            if(board[row1-1][col1].equals("##") || board[row1-1][col1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1-1][col1+1].equals("##") || board[row1-1][col1+1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1-1][col1-1].equals("##") || board[row1-1][col1-1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1+1][col1].equals("##") || board[row1+1][col1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1+1][col1-1].equals("##") || board[row1+1][col1-1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1+1][col1+1].equals("##") || board[row1+1][col1+1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1][col1+1].equals("##") || board[row1][col1+1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }
        try
        {
            if(board[row1][col1-1].equals("##") || board[row1-1][col1-1].equals("  "))
                king++;
        }
        catch (IndexOutOfBoundsException e)
        {
            filled++;
        }

        if( row1!=0 && board[row1-1][col1].charAt(0)!= color) {
            if( checkPawn( color, row1-1, col1 )   ||
                    checkRook( color, row1-1, col1)    ||
                    checkKnight( color, row1-1, col1)  ||
                    checkQueen( color, row1-1, col1 )  ||
                    checkKing( color, row1-1, col1 )   ||
                    checkBishop( color, row1-1, col1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 1;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( row1!=0 && col1!=7 && board[row1-1][col1+1].charAt(0)!= color) {

            if( checkPawn( color, row1-1, col1+1 )   ||
                    checkRook( color, row1-1, col1+1 )   ||
                    checkKnight( color, row1-1, col1+1 ) ||
                    checkQueen( color, row1-1, col1+1 )  ||
                    checkKing( color, row1-1, col1+1 )   ||
                    checkBishop( color, row1-1, col1+1 ) )
            {
                p++;
                if(p == king) {
                    tracker  = 2;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }
        if( col1!=7 && board[row1][col1+1].charAt(0)!= color ) {
            if( checkPawn( color, row1, col1+1 )   ||
                    checkRook( color, row1, col1+1 )   ||
                    checkKnight( color, row1, col1+1 ) ||
                    checkQueen( color, row1, col1+1 )  ||
                    checkKing( color, row1, col1+1 )   ||
                    checkBishop( color, row1, col1+1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 3;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( row1!=7 && col1!=7 && board[row1+1][col1+1].charAt(0)!= color ) {
            if( checkPawn( color, row1+1, col1+1 )   ||
                    checkRook( color, row1+1, col1+1 )   ||
                    checkKnight( color, row1+1, col1+1 ) ||
                    checkQueen( color, row1+1, col1+1 )  ||
                    checkKing( color, row1+1, col1+1 )   ||
                    checkBishop( color, row1+1, col1+1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 4;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( row1!=7 && board[row1+1][col1].charAt(0)!= color) {
            if( checkPawn( color, row1+1, col1 )   ||
                    checkRook( color, row1+1, col1 )   ||
                    checkKnight( color, row1+1, col1 ) ||
                    checkQueen( color, row1+1, col1 )  ||
                    checkKing( color, row1+1, col1 )   ||
                    checkBishop( color, row1+1, col1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 5;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( row1!=7 && col1!=0 && board[row1+1][col1-1].charAt(0)!= color ) {
            if(	checkPawn( color, row1+1, col1-1 )   ||
                    checkRook( color, row1+1, col1-1 )   ||
                    checkKnight( color, row1+1, col1-1 ) ||
                    checkQueen( color, row1+1, col1-1 )  ||
                    checkKing( color, row1+1, col1-1 )   ||
                    checkBishop( color, row1+1, col1-1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 6;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( col1!=0 && board[row1][col1-1].charAt(0)!= color ) {
            if(	checkPawn( color, row1, col1-1 )   ||
                    checkRook( color, row1, col1-1 )   ||
                    checkKnight( color, row1, col1-1 ) ||
                    checkQueen( color, row1, col1-1 )  ||
                    checkKing( color, row1, col1-1 )   ||
                    checkBishop( color, row1, col1-1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 7;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }

        if( row1!=0 && col1!=0 && board[row1-1][col1-1].charAt(0)!= color ) {
            if( checkPawn( color, row1-1, col1-1 )   ||
                    checkRook( color, row1-1, col1-1 )   ||
                    checkKnight( color, row1-1, col1-1 ) ||
                    checkQueen( color, row1-1, col1-1 )  ||
                    checkKing( color, row1-1, col1-1 )   ||
                    checkBishop( color, row1-1, col1-1 ) )
            {
                p++;
                if(p == king) {
                    tracker = 8;
                    possibleCheckMate = true;
                    return p;
                }
            }
        }
        possibleCheckMate = false;    // king is not in check mate
        return p;


    }

    /**
     * Check to see if king will be checked by a knight if moved
     *
     * @param color determines the color of the piece
     * @param row1 row of the opponent's king
     * @param col1 column of the opponent's king
     * @return true if king will be checked by knight
     */
    private static boolean checkKnight( char color, int row1, int col1 ) {

        if( row1!=0 && row1!=1 && col1!=7 ) {

            if( board[row1-2][col1+1].charAt(0)!=color && board[row1-2][col1+1].charAt(1) == 'n' ) {
                threatPiece = "n -2 +1";
                return true;
            }
        }

        if( row1!=0 && row1!=1 && col1!=0) {
            if( board[row1-2][col1-1].charAt(0)!=color && board[row1-2][col1-1].charAt(1)== 'n' ) {
                threatPiece = "n -2 -1";
                return true;
            }
        }

        if( row1!=0 && col1!=6 && col1!=7) {
            if( board[row1-1][col1+2].charAt(0)!=color && board[row1-1][col1+2].charAt(1)== 'n' ) {
                threatPiece = "n -1 +2";
                return true;
            }
        }

        if( row1!=0 && col1!=0 && col1!=1) {
            if( board[row1-1][col1-2].charAt(0)!=color && board[row1-1][col1-2].charAt(1)== 'n' ) {
                threatPiece = "n -1 -2";
                return true;
            }
        }

        if( row1!=6 && row1!=7 && col1!=0 ) {
            if( board[row1+2][col1-1].charAt(0)!=color && board[row1+2][col1-1].charAt(1)== 'n' ) {
                threatPiece = "n +2 -1";
                return true;
            }
        }

        if( row1!=6 && row1!=7 && col1!=7) {
            if( board[row1+2][col1+1].charAt(0)!=color && board[row1+2][col1+1].charAt(1)== 'n' ) {
                threatPiece = "n +2 +1";
                return true;
            }
        }

        if( row1!=7 && col1!=0 && col1!=1) {
            if( board[row1+1][col1-2].charAt(0)!=color && board[row1+1][col1-2].charAt(1)== 'n' ) {
                threatPiece = "n +1 -2";
                return true;
            }
        }

        if( row1!=7 && col1!=6 && col1!=7) {
            if( board[row1+1][col1+2].charAt(0)!=color && board[row1+1][col1+2].charAt(1)== 'n' ) {
                threatPiece = "n +1 +2";
                return true;
            }
        }

        return false;



    }

    /**
     * Check to see if king will be checked by opponent's king if moved
     *
     * @param color determines the color of the piece
     * @param row1 row of opponent's king
     * @param col1 column of opponent's king
     * @return true if king will be checked by opponent's king
     */
    private static boolean checkKing( char color, int row1, int col1 ) {

        if( row1!=0 ) {
            if( board[row1-1][col1].charAt(0)!=color && board[row1-1][col1].charAt(1) == 'n' ) {
                threatPiece = "k -1 +0";
                return true;
            }
        }

        if( row1!=0 && col1!=7 ) {
            if( board[row1-1][col1+1].charAt(0)!=color && board[row1-1][col1+1].charAt(1) == 'n' ) {
                threatPiece = "k -1 +1";
                return true;
            }
        }

        if( col1!=7 ) {
            if( board[row1][col1+1].charAt(0)!=color && board[row1][col1+1].charAt(1) == 'n' ) {
                threatPiece = "k +0 +1";
                return true;
            }
        }

        if( row1!=7 && col1!=7 ) {
            if( board[row1+1][col1+1].charAt(0)!=color && board[row1+1][col1+1].charAt(1) == 'n' ) {
                threatPiece = "k +1 +1";
                return true;
            }
        }

        if( row1!=7 ) {
            if( board[row1+1][col1].charAt(0)!=color && board[row1+1][col1].charAt(1) == 'n' ) {
                threatPiece = "k +1 +0";
                return true;
            }
        }

        if( row1!=7 && col1!=0 ) {
            if( board[row1+1][col1-1].charAt(0)!=color && board[row1+1][col1-1].charAt(1) == 'n' ) {
                threatPiece = "k +1 -1";
                return true;
            }
        }

        if( col1!=0 ) {
            if( board[row1][col1-1].charAt(0)!=color && board[row1][col1-1].charAt(1) == 'n' ) {
                threatPiece = "k +0 -1";
                return true;
            }
        }

        if( row1!=0 && col1!=0 ) {
            if( board[row1-1][col1-1].charAt(0)!=color && board[row1-1][col1-1].charAt(1) == 'n' ) {
                threatPiece = "k -1 -1";
                return true;
            }
        }

        return false;


    }

    /**
     * Check to see if king will be checked by pawn if moved
     *
     * @param c determines the color of the piece
     * @param row2 row of opponent's king
     * @param col2 column of opponent's king
     * @return true if king will be checked by pawn if moved
     */
    private static boolean checkPawn(char c, int row2, int col2)
    {
        //piece is white King
        if(c == 'w')
        {
            if(row2 == 0) {
                return false;
            }
            else if(col2 == 0) {
                if(board[row2-1][col2+1].equals("bp")) {
                    threatPiece = "p -1 +1";
                    return true;
                }
            }
            else if(col2 ==7) {
                if(board[row2-1][col2-1].equals("bp")) {
                    threatPiece = "p -1 -1";
                    return true;
                }
            }
            else if(board[row2-1][col2+1].equals("bp") || board[row2-1][col2-1].equals("bp")) {
                return true;
            }
            else if( board[row2-2][col2]=="bp" && row2-2==1) {
                return true;
            } else {
                return false;
            }
        }
        else //piece is black King
        {
            if(row2 == 0) {
                return false;
            } else if(col2 == 0) {
                if(board[row2+1][col2+1].equals("wp")) {
                    threatPiece = "p +1 +1";
                    return true;
                }
            }
            else if(col2 ==7) {
                if(board[row2+1][col2-1].equals("wp")) {
                    threatPiece = "p +1 -1";
                    return true;
                }
            }
            else if(board[row2+1][col2+1].equals("wp") || board[row2+1][col2-1].equals("wp")) {
                return true;
            } else if( board[row2+2][col2]=="wp" && row2+2==6) {
                return true;
            } else {
                return false;
            }
        } return false;
    }

    /**
     * Check to see if king will be checked by rook if moved
     *
     * @param color determines color of piece
     * @param row1 row of opponent's king
     * @param col1 column of opponent's king
     * @return true if king will be checked by rook
     */
    private static boolean checkRook( char color, int row1, int col1 ) {

        if( col1!=7 && col1!=0 && row1!=0 && row1!=7 ) {
            // check right side for Rook threat
            for( int j = col1+1; j<=7; j++ ) {
                if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                    if( board[row1][j].charAt(1) == 'r' && board[row1][j].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[row1][j].charAt(1)=='r'||board[row1][j].charAt(1)=='q')&& board[row1][j].charAt(0)!=color ) {
                        threatPiecerow = row1; threatPiececol = j;
                        return true;
                    } else { break; }
                } else { continue; }
            }
            // check left side for Rook threat
            for( int j = col1-1; j>=0; j-- ) {
                if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                    if( board[row1][j].charAt(1) == 'k' && board[row1][j].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[row1][j].charAt(1)=='r' || board[row1][j].charAt(1)=='q') && board[row1][j].charAt(0)!=color ) {
                        threatPiecerow = row1; threatPiececol = j;
                        return true;
                    } else { break; }
                } else { continue; }
            }

            // check down the columns for Rook threat
            for( int i = row1+1; i<=7; i++ ) {
                if( board[i][col1].charAt(0)=='w' || board[i][col1].charAt(0)=='b' ) {
                    if( board[i][col1].charAt(1) == 'k' && board[i][col1].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[i][col1].charAt(1)=='r' || board[i][col1].charAt(1)=='q') && board[i][col1].charAt(0)!=color ) {
                        threatPiecerow = i; threatPiececol = col1;
                        return true;
                    } else { break; }
                } else { continue; }
            }

            // check up the columns for Rook threat
            for( int i = row1-1; i>=0; i-- ) {
                if( board[i][col1].charAt(0)=='w' || board[i][col1].charAt(0)=='b' ) {
                    if( board[i][col1].charAt(1) == 'k' && board[i][col1].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[i][col1].charAt(1)=='r' || board[i][col1].charAt(1)=='q') && board[i][col1].charAt(0)!=color ) {
                        threatPiecerow = i; threatPiececol = col1;
                        return true;
                    } else { break; }
                } else { continue; }
            }

        }

        if( row1==0 ) {
            if( col1!=0 ) {
                // check left side for Rook threat
                for( int j = col1-1; j>=0; j-- ) {
                    if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                        if( board[row1][j].charAt(1) == 'k' && board[row1][j].charAt(0) == color ) {
                            continue;
                        }
                        if( (board[row1][j].charAt(1)=='r' || board[row1][j].charAt(1)=='q') && board[row1][j].charAt(0)!=color ) {
                            threatPiecerow = row1; threatPiececol = j;
                            return true;
                        } else { break; }
                    } else { continue; }
                }
            }

            // check down the columns for Rook threat
            for( int i = row1+1; i<=7; i++ ) {
                if( board[i][col1].charAt(0)=='w' || board[i][col1].charAt(0)=='b' ) {
                    if( board[i][col1].charAt(1) == 'k' && board[i][col1].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[i][col1].charAt(1)=='r' || board[i][col1].charAt(1)=='q') && board[i][col1].charAt(0)!=color ) {
                        threatPiecerow = i; threatPiececol = col1;
                        return true;
                    } else { break; }
                } else { continue; }
            }


            // check right side for Rook threat
            if( col1!=7 ) {
                for( int j = col1+1; j<=7; j++ ) {
                    if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                        if( board[row1][j].charAt(1) == 'k' && board[row1][j].charAt(0) == color ) {
                            continue;
                        }
                        if( (board[row1][j].charAt(1)=='r'||board[row1][j].charAt(1)=='q') && board[row1][j].charAt(0)!=color ) {
                            threatPiecerow = row1; threatPiececol = j;
                            return true;
                        } else { break; }
                    } else { continue; }
                }
            }

        }


        if( row1==7 ) {
            // check left side for Rook threat
            if( col1!=0 ) {
                for( int j = col1-1; j>=0; j-- ) {
                    if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                        if( board[row1][j].charAt(1) == 'k' && board[row1][j].charAt(0) == color ) {
                            continue;
                        }
                        if( (board[row1][j].charAt(1)=='r' || board[row1][j].charAt(1)=='q') && board[row1][j].charAt(0)!=color ) {
                            threatPiecerow = row1; threatPiececol = j;
                            return true;
                        } else { break; }
                    } else { continue; }
                }
            }

            // check right side for Rook threat
            if( col1!=7 ) {
                for( int j = col1+1; j<=7; j++ ) {
                    if( board[row1][j].charAt(0)=='w' || board[row1][j].charAt(0)=='b' ) {
                        if( board[row1][j].charAt(1) == 'k' && board[row1][j].charAt(0) == color ) {
                            continue;
                        }
                        if( (board[row1][j].charAt(1)=='r' || board[row1][j].charAt(1)=='q') && board[row1][j].charAt(0)!=color ) {
                            threatPiecerow = row1; threatPiececol = j;
                            return true;
                        } else { break; }
                    } else { continue; }
                }
            }

            // check up the columns for Rook threat
            for( int i = row1-1; i>=0; i-- ) {
                if( board[i][col1].charAt(0)=='w' || board[i][col1].charAt(0)=='b' ) {
                    if( board[i][col1].charAt(1) == 'k' && board[i][col1].charAt(0) == color ) {
                        continue;
                    }
                    if( (board[i][col1].charAt(1)=='r' || board[i][col1].charAt(1)=='q') && board[i][col1].charAt(0)!=color ) {
                        threatPiecerow = i; threatPiececol = col1;
                        return true;
                    } else { break; }
                } else { continue; }
            }

        }

        return false;

    }

    /**
     * Check to see if king will be checked by bishop if moved
     *
     * @param j determines the color of the piece
     * @param row2 row of king
     * @param col2 column of king
     * @return true is king will be checked
     */
    private static boolean checkBishop(char j, int row2, int col2)
    {
        int k=0;
        if(row2 == 0)
        {
            if(col2 == 0)
            {
                for(int i = 1; i < 8; i++)
                {
                    row2++;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else { break; }
                    }
                }
            }
            else if(col2 == 7)
            {
                for(int i = 6; i >= 0; i--)
                {
                    row2++;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else { break; }
                    }
                }
            }
            else
            {
                int c = row2;
                for(int i = col2+1; i < 8; i++)
                {
                    c++;
                    if(board[c][i].charAt(0) == 'b' || board[c][i].charAt(0) == 'w')
                    {
                        if( board[c][i].charAt(1) == 'k' && board[c][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[c][i].charAt(1)=='q'||board[c][i].charAt(1)=='b') && board[c][i].charAt(0)!=j) {
                            threatPiecerow = c; threatPiececol = i;
                            return true;
                        } else { break; }
                    }
                }
                for(int i = col2-1; i >= 0; i--)
                {
                    row2++;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else{ break; }
                    }
                }
            }
        }
        else if(row2 == 7)
        {
            if(col2 == 0)
            {
                for(int i = col2+1; i < 8; i++)
                {
                    row2--;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else { break; }
                    }
                }
            }
            else if(col2 == 7)
            {
                for(int i = col2-1; i >= 0; i--)
                {
                    row2--;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else { break; }
                    }
                }
            }
            else
            {
                int c = row2;
                for(int i = col2+1; i < 8; i++)
                {
                    c--;
                    if(board[c][i].charAt(0) == 'b' || board[c][i].charAt(0) == 'w')
                    {
                        if( board[c][i].charAt(1) == 'k' && board[c][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[c][i].charAt(1)=='q'||board[c][i].charAt(1)=='b') && board[c][i].charAt(0)!=j) {
                            threatPiecerow = c; threatPiececol = i;
                            return true;
                        } else { break;	}
                    }
                }
                for(int i = col2-1; i >= 0; i--)
                {
                    row2--;
                    if(board[row2][i].charAt(0) == 'b' || board[row2][i].charAt(0) == 'w')
                    {
                        if( board[row2][i].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                            continue;
                        }
                        if((board[row2][i].charAt(1)=='q'||board[row2][i].charAt(1)=='b') && board[row2][i].charAt(0)!=j) {
                            threatPiecerow = row2; threatPiececol = i;
                            return true;
                        } else{ break; }
                    }
                }
            }
        }
        else if(col2 == 0)
        {
            int c = col2;
            for(int i = row2+1; i < 8; i++)
            {
                c++;
                if(board[i][c].charAt(0) == 'b' || board[i][c].charAt(0) == 'w')
                {
                    if( board[i][c].charAt(1) == 'k' && board[i][c].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][c].charAt(1)=='q'||board[i][c].charAt(1)=='b') && board[i][c].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = c;
                        return true;
                    } else{ break; }
                }
            }
            for(int i = row2-1; i >= 0; i--)
            {
                col2++;
                if(board[i][col2].charAt(0) == 'b' || board[i][col2].charAt(0) == 'w')
                {
                    if( board[i][col2].charAt(1) == 'k' && board[i][col2].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][col2].charAt(1)=='q'||board[i][col2].charAt(1)=='b') && board[i][col2].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = col2;
                        return true;
                    } else { break; }
                }
            }
        }
        else if(col2 == 7)
        {
            int c = col2;
            for(int i = row2+1; i < 8; i++)
            {
                c--;
                if(board[i][c].charAt(0) == 'b' || board[i][c].charAt(0) == 'w')
                {
                    if( board[i][c].charAt(1) == 'k' && board[i][c].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][c].charAt(1)=='q'||board[i][c].charAt(1)=='b') && board[i][c].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = c;
                        return true;
                    } else { break; }
                }
            }
            for(int i = row2-1; i >= 0; i--)
            {
                col2--;
                if(board[i][col2].charAt(0) == 'b' || board[i][col2].charAt(0) == 'w')
                {
                    if( board[i][col2].charAt(1) == 'k' && board[i][col2].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][col2].charAt(1)=='q'||board[i][col2].charAt(1)=='b') && board[i][col2].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = col2;
                        return true;
                    } else { break; }
                }
            }
        }
        else
        {
            int c = col2;
            for(int i = row2+1; i < 8; i++)
            {
                c++;
                if(c > 7)
                {
                    break;
                }
                if(board[i][c].charAt(0) == 'b' || board[i][c].charAt(0) == 'w')
                {
                    if( board[i][c].charAt(1) == 'k' && board[i][c].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][c].charAt(1)=='q'||board[i][c].charAt(1)=='b') && board[i][c].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = c;
                        return true;
                    } else { break; }
                }
            }
            int a = col2;
            for(int i = row2-1; i >= 0; i--)
            {
                a++;
                if(a > 7)
                {
                    break;
                }
                if(board[i][a].charAt(0) == 'b' || board[i][a].charAt(0) == 'w')
                {
                    if( board[i][a].charAt(1) == 'k' && board[i][a].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][a].charAt(1)=='q'||board[i][a].charAt(1)=='b') && board[i][a].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = a;
                        return true;
                    } else { break; }
                }
            }
            int b = col2;
            for(int i = row2+1; i < 8; i++)
            {
                b--;
                if(b < 0)
                {
                    break;
                }
                if(board[i][b].charAt(0) == 'b' || board[i][b].charAt(0) == 'w')
                {
                    if( board[i][b].charAt(1) == 'k' && board[i][b].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][b].charAt(1)=='q'||board[i][b].charAt(1)=='b') && board[i][b].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = b;
                        return true;
                    } else { break; }
                }
            }
            int d = col2;
            for(int i = row2-1; i >= 0; i--)
            {
                d++;
                if(d > 7)
                {
                    break;
                }
                if(board[i][d].charAt(0) == 'b' || board[i][d].charAt(0) == 'w')
                {
                    if( board[i][d].charAt(1) == 'k' && board[row2][i].charAt(0) == j ) {
                        continue;
                    }
                    if((board[i][d].charAt(1)=='q'||board[i][d].charAt(1)=='b') && board[i][d].charAt(0)!=j) {
                        threatPiecerow = i; threatPiececol = d;
                        return true;
                    } else { break; }
                }
            }

        }
        return false;

    }

    /**
     * Check to see if king will be checked by queen if moved
     *
     * @param j determines the color of the piece
     * @param row2 row of king
     * @param col2 column of king
     * @return true if king will be checked by queen
     */
    private static boolean checkQueen(char j, int row2, int col2)
    {
        if(checkBishop(j, row2,col2) == true || checkRook(j, row2,col2) == true)
            return true;
        return false;
    }

    /*
     * Check to see if the threat piece can be eaten
     *
     * @param color determines the color of the piece
     * @param location of the threat piece in String form
     *
     */
    private static boolean ThreatPiece( char color, String threatPiece ) {
        if( threatPiece!=null ) {
            char piece = threatPiece.charAt(0);
            char sign1 = threatPiece.charAt(2);
            int row1 = threatPiece.charAt(3);
            char sign2 = threatPiece.charAt(5);
            int col1 = threatPiece.charAt(6);

            if( sign1 == '-' ) { row1 = -1*row1; }
            if( sign2 == '-' ) { col1 = -1*col1; }

            if( color=='b' && (piece == 'n' || piece == 'p' || piece=='k') ) {
                if( checkPawn('w', wKingrow+row1, wKingcol+col1)==true ||
                        checkKnight('w', wKingrow+row1, wKingcol+col1)==true ||
                        checkKing('w', wKingrow+row1, wKingcol+col1)==true ||
                        checkRook('w', wKingrow+row1, wKingcol+col1)==true ||
                        checkBishop('w', wKingrow+row1, wKingcol+col1)==true ||
                        checkQueen('w', wKingrow+row1, wKingcol+col1)==true ) {
                    return false;				 // the threat piece can be eaten, so it's not checkMate
                } else { return true; }
            }

            if( color=='w' && (piece == 'n' || piece == 'p' || piece=='k') ) {
                if( checkPawn('b', bKingrow+row1, bKingcol+col1)==true ||
                        checkKnight('b', bKingrow+row1, bKingcol+col1)==true ||
                        checkKing('b', bKingrow+row1, bKingcol+col1)==true ||
                        checkRook('b', bKingrow+row1, bKingcol+col1)==true ||
                        checkBishop('b', bKingrow+row1, bKingcol+col1)==true ||
                        checkQueen('b', bKingrow+row1, bKingcol+col1)==true ) {
                    return false;				 // the threat piece can be eaten, so it's not checkMate
                } else { return true; }
            }
        } else {
            char j = board[threatPiecerow][threatPiececol].charAt(0);
            if( color == 'b' ) {
                if( checkPawn( j, threatPiecerow, threatPiececol)==true ||
                        checkKnight(j, threatPiecerow, threatPiececol)==true ||
                        checkKing(j, threatPiecerow, threatPiececol)==true ||
                        checkRook(j, threatPiecerow, threatPiececol)==true ||
                        checkBishop(j, threatPiecerow, threatPiececol)==true ||
                        checkQueen(j, threatPiecerow, threatPiececol)==true ) {
                    return false;				 // the threat piece can be eaten, so it's not checkMate
                } else { return true; }
            }
            if( color=='w' ) {
                if( checkPawn(j, threatPiecerow, threatPiececol)==true ||
                        checkKnight(j, threatPiecerow, threatPiececol)==true ||
                        checkKing(j, threatPiecerow, threatPiececol)==true ||
                        checkRook(j, threatPiecerow, threatPiececol)==true ||
                        checkBishop(j, threatPiecerow, threatPiececol)==true ||
                        checkQueen(j, threatPiecerow, threatPiececol)==true ) {
                    return false;				 // the threat piece can be eaten, so it's not checkMate
                } else { return true; }
            }
        }
        return false;

    }



    public boolean trycheckMate() {
        if ((checkMate('b', bKingrow, bKingcol) == 1) && possibleCheckMate == true) {
            if (ThreatPiece('b', threatPiece) == true) {
                possibleCheckMate = true;
                System.out.println("");
                System.out.println("Checkmate");
                System.out.println("White wins");
                return true;
                //reader.close(); System.exit(0);
            } else {
                bKingCheck2 = true;
                threatPiece = "  ";
                threatPiececol = -1;
                threatPiecerow = -1;
            }
        } else if (checkMate('b', bKingrow, bKingcol) > 1 && possibleCheckMate == true) {
            System.out.println("");
            System.out.println("Checkmate");
            System.out.println("White wins");
            return true;
            //reader.close(); System.exit(0);
        }

        if( (checkMate('w', wKingrow, wKingcol) == 1) && possibleCheckMate==true )
        {
            if( ThreatPiece( 'w', threatPiece ) == true ) {
                possibleCheckMate = true;
                System.out.println("");
                System.out.println("Checkmate");
                System.out.println("Black wins");
                //reader.close(); System.exit(0);
                return true;
            } else {
                wKingCheck1 = true;
                threatPiece = "  ";
                threatPiececol = -1; threatPiecerow = -1;
            }
        } else if ( checkMate('w', wKingrow, wKingcol)>1 && possibleCheckMate==true) {
            System.out.println("");
            System.out.println("Checkmate");
            System.out.println("Black wins");
            return true;
            //reader.close(); System.exit(0);
        }
        return false;
    }

    public boolean tryCheck() {
        if( checkPawn('w', wKingrow, wKingcol) == true ||
                checkRook('w', wKingrow, wKingcol) == true ||
                checkKnight('w', wKingrow, wKingcol) == true ||
                checkQueen('w', wKingrow, wKingcol) == true ||
                checkKing('w', wKingrow, wKingcol) == true ||
                checkBishop('w', wKingrow, wKingcol) == true ) {
            wKingCheck2 = true;
        } else { wKingCheck2 = false; }
        if( checkPawn('b', bKingrow, bKingcol) == true ||
                checkRook('b', bKingrow, bKingcol) == true ||
                checkKnight('b', bKingrow, bKingcol) == true ||
                checkQueen('b', bKingrow, bKingcol) == true ||
                checkKing('b', bKingrow, bKingcol) == true ||
                checkBishop('b', bKingrow, bKingcol) == true) {
            bKingCheck2 = true;
        } else { bKingCheck2 = false; }
        if( bKingCheck2 == true || wKingCheck2 ==true ) { System.out.println(" "); System.out.println( "Check" ); return true; }
        else { return false; }

        /*if(end == true)
        {
            reader.close();
            System.exit(0);
        }*/
    }
    /*public static void main(String[] args) {

        fillBoard();
        System.out.println("");
        drawBoard();
        while(end == false)
        {	//white's turn
            Scanner reader = new Scanner(System.in);
            if(counter%2 == 0)
            {
                System.out.println("");
                System.out.print("White's move: ");
                String input = reader.nextLine();

                //check to see if input is valid
                if(input.length() < 5 || input.length() > 11)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                if(input.length() > 5)
                {
                    if(input.substring(6).equals("draw?"))
                    {
                        char a = input.charAt(0);
                        int n = Character.getNumericValue(input.charAt(1));
                        int col = convertToArray(a);
                        int row = convertToArray(n);
                        if(board[row][col].charAt(0) != 'w')
                        {
                            System.out.println("Illegal move, try again");
                            continue;
                        }
                        String input2 = reader.nextLine();
                        if(input2.equals("draw"))
                        {
                            System.exit(0);
                        }
                    }
                    if(input.equals("resign"))
                    {
                        System.out.println("Black wins");
                        System.exit(0);
                    }
                }
                char[] letters = {'a','b','c','d','e','f','g','h'};
                if(Arrays.binarySearch(letters,input.charAt(0)) < 0 || Arrays.binarySearch(letters,input.charAt(3)) < 0)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                int[] nums = {1,2,3,4,5,6,7,8};
                if(Arrays.binarySearch(nums,Character.getNumericValue(input.charAt(1))) < 0 || Arrays.binarySearch(nums,Character.getNumericValue(input.charAt(4))) < 0)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                char l1 = input.charAt(0);
                int n1 = Character.getNumericValue(input.charAt(1));
                char l2 = input.charAt(3);
                int n2 = Character.getNumericValue(input.charAt(4));
                char p;
                if(input.length() == 7 && input.charAt(5) == ' ')
                    p = input.charAt(6);
                else if(input.length() == 7 && input.charAt(5) != ' ')
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                else
                    p = ' ';
                int col1 = convertToArray(l1);
                int row1 = convertToArray(n1);
                int col2 = convertToArray(l2);
                int row2 = convertToArray(n2);
                String temp = board[row1][col1];
                char c = temp.charAt(0);
                if(c != 'w')
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                movePiece(row1, col1, row2, col2, p);
                if( (checkMate('b', bKingrow, bKingcol) == 1) && possibleCheckMate==true )
                {
                    if( ThreatPiece( 'b', threatPiece ) == true ) {
                        possibleCheckMate = true;
                        System.out.println("");
                        System.out.println( "Checkmate");
                        System.out.println("White wins");
                        reader.close(); System.exit(0);
                    } else {
                        bKingCheck2 = true;
                        threatPiece = "  ";
                        threatPiececol = -1; threatPiecerow = -1;
                    }
                } else if(checkMate('b', bKingrow, bKingcol) > 1 && possibleCheckMate==true ){
                    System.out.println("");
                    System.out.println("Checkmate");
                    System.out.println("White wins");
                    reader.close(); System.exit(0);
                }
                if( checkPawn('w', wKingrow, wKingcol) == true ||
                        checkRook('w', wKingrow, wKingcol) == true ||
                        checkKnight('w', wKingrow, wKingcol) == true ||
                        checkQueen('w', wKingrow, wKingcol) == true ||
                        checkKing('w', wKingrow, wKingcol) == true ||
                        checkBishop('w', wKingrow, wKingcol) == true ) {
                    wKingCheck2 = true;
                } else { wKingCheck2 = false; }
                if( checkPawn('b', bKingrow, bKingcol) == true ||
                        checkRook('b', bKingrow, bKingcol) == true ||
                        checkKnight('b', bKingrow, bKingcol) == true ||
                        checkQueen('b', bKingrow, bKingcol) == true ||
                        checkKing('b', bKingrow, bKingcol) == true ||
                        checkBishop('b', bKingrow, bKingcol) == true) {
                    bKingCheck2 = true;
                } else { bKingCheck2 = false; }
                if( bKingCheck2 == true || wKingCheck2 ==true ) { System.out.println(" "); System.out.println( "Check" ); }

                if(end == true)
                {
                    reader.close();
                    System.exit(0);
                }
            }
            else
            {
                System.out.println("");
                System.out.print("Black's move: ");
                String input2 = reader.nextLine();

                //check to see if input is valid
                if(input2.length() < 5 || input2.length() > 11)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                if(input2.length() > 5)
                {
                    if(input2.substring(6).equals("draw?"))
                    {
                        char a = input2.charAt(0);
                        int n = Character.getNumericValue(input2.charAt(1));
                        int col = convertToArray(a);
                        int row = convertToArray(n);
                        if(board[row][col].charAt(0) != 'b')
                        {
                            System.out.println("Illegal move, try again");
                            continue;
                        }
                        String input3 = reader.nextLine();
                        if(input3.equals("draw"))
                        {
                            System.exit(0);
                        }
                    }
                    if(input2.equals("resign"))
                    {
                        System.out.println("White wins");
                        System.exit(0);
                    }
                }
                char[] chars = {'a','b','c','d','e','f','g','h'};
                if(Arrays.binarySearch(chars,input2.charAt(0)) < 0 || Arrays.binarySearch(chars,input2.charAt(3)) < 0)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                int[] numbers = {1,2,3,4,5,6,7,8};
                if(Arrays.binarySearch(numbers,Character.getNumericValue(input2.charAt(1))) < 0 || Arrays.binarySearch(numbers,Character.getNumericValue(input2.charAt(4))) < 0)
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                char l3 = input2.charAt(0);
                int n3 = Character.getNumericValue(input2.charAt(1));
                char l4 = input2.charAt(3);
                int n4 = Character.getNumericValue(input2.charAt(4));
                char p2;
                if(input2.length() == 7 && input2.charAt(5) == ' ')
                    p2 = input2.charAt(6);
                else if(input2.length() == 7 && input2.charAt(5) != ' ')
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                else
                    p2 = ' ';
                int col3 = convertToArray(l3);
                int row3 = convertToArray(n3);
                int col4 = convertToArray(l4);
                int row4 = convertToArray(n4);
                String temp = board[row3][col3];
                char c = temp.charAt(0);
                if(c != 'b')
                {
                    System.out.println("Illegal move, try again");
                    continue;
                }
                movePiece(row3, col3, row4, col4, p2);
                if( (checkMate('w', wKingrow, wKingcol) == 1) && possibleCheckMate==true )
                {
                    if( ThreatPiece( 'w', threatPiece ) == true ) {
                        possibleCheckMate = true;
                        System.out.println("");
                        System.out.println("Checkmate");
                        System.out.println("Black wins");
                        reader.close(); System.exit(0);
                    } else {
                        wKingCheck1 = true;
                        threatPiece = "  ";
                        threatPiececol = -1; threatPiecerow = -1;
                    }
                } else if ( checkMate('w', wKingrow, wKingcol)>1 && possibleCheckMate==true) {
                    System.out.println("");
                    System.out.println("Checkmate");
                    System.out.println("Black wins");
                    reader.close(); System.exit(0);
                }

                if( checkPawn('w', wKingrow, wKingcol) == true ||
                        checkRook('w', wKingrow, wKingcol) == true ||
                        checkKnight('w', wKingrow, wKingcol) == true ||
                        checkQueen('w', wKingrow, wKingcol) == true ||
                        checkKing('w', wKingrow, wKingcol) == true ||
                        checkBishop('w', wKingrow, wKingcol) == true ) {
                    wKingCheck1 = true;
                } else { wKingCheck1 = false; }
                if( checkPawn('b', bKingrow, bKingcol) == true ||
                        checkRook('b', bKingrow, bKingcol) == true ||
                        checkKnight('b', bKingrow, bKingcol) == true ||
                        checkQueen('b', bKingrow, bKingcol) == true ||
                        checkKing('b', bKingrow, bKingcol) == true ||
                        checkBishop('b', bKingrow, bKingcol) == true) {
                    bKingCheck1 = true;
                } else { bKingCheck1 = false; }
                if( wKingCheck1==true || bKingCheck1==true ) { System.out.println(" "); System.out.println( "Check" ); }
                if(end == true)
                {
                    reader.close();
                    System.exit(0);
                }
            }

        }
        System.exit(0);
    } */
}


